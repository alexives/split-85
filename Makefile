LEFT=$(shell grep -o 'export .*' src/left.scad | sed 's,export ,out/,g')
RIGHT=$(shell grep -o 'export .*' src/right.scad | sed 's,export ,out/,g')
TARGETS=${LEFT} ${RIGHT}

all: ${TARGETS}

.SECONDARY: $(shell echo "${TARGETS}" | sed 's/\.stl/.scad/g')

include $(wildcard *.deps)

list:
	echo ${TARGETS}

tmp/%.scad:
	mkdir -p tmp
	echo "use <../src/$(shell echo $* | sed 's/_.*//g').scad>\n$*();" > $@

out/%.stl: tmp/%.scad
	mkdir -p out
	openscad -o $@ tmp/$*.scad

out/%.svg: tmp/%.scad
	mkdir -p out
	openscad -o $@ tmp/$*.scad

clean:
	rm -rf tmp/* out/*
