use <../keyboard_lib/keyboard.scad>

module case_rm() {
  translate([0,-u_mm(2.5)-0.25])
    square([u_mm(1), u_mm(3.5)+0.5]);
  translate([-u_mm(0.5),u_mm(1)-0.25])
    square([u_mm(1), u_mm(1)+0.5]);
  translate([-u_mm(0.25),u_mm(2)-0.25])
    square([u_mm(1), u_mm(1)+0.5]);
  translate([u_mm(0.25),u_mm(3)-0.25])
    square([u_mm(1), u_mm(4.5)+0.5]);
}