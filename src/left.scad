include <common.scad>

module case() {
  translate([0,-u_mm(1.5)])
    square([u_mm(7), u_mm(2.5)]);
  translate([0,u_mm(1)])
    square([u_mm(6.5), u_mm(1)]);
  translate([0,u_mm(2)])
    square([u_mm(6.75), u_mm(1)]);
  translate([0,u_mm(3)])
    square([u_mm(7.25), u_mm(2.25)]);
}

function layout() = [
  [[],[],[],[],[],[],[]],
  [[1.5],[],[],[],[],[]],
  [[1.75],[],[],[],[],[]],
  [[2.25],[],[],[],[],[]],
  [[1.25],[1.25],[1.25],[2.25],[1.25]]
];

module remove_3d_edge() {
  difference(){
    children();
    translate([u_mm(7),0,-20])
      linear_extrude(40)
        case_rm();
  }
}

module remove_2d_edge() {
  difference(){
    children();
    translate([u_mm(7),0])
      case_rm();
  }
}

module left_board() { // export left_board.stl
  remove_3d_edge() {
    board(layout()){
      case();
    }
  }
}

module left_top_case() { // export left_top_case.stl
  remove_3d_edge() {
    top_case(layout(), margin=8, depth=6){
      case();
    }
  }
}

module left_bottom_case() { // export left_bottom_case.stl
  remove_3d_edge() {
    bottom_case(layout(), margin=8, depth=6){
      case();
    }
  }
}

module left_plate() { // export left_plate.svg
  remove_2d_edge() {
    top_plate_drawing(layout()){
      case();
    }
  }
}

module left_pcb_outline() { // export left_pcb_outline.svg
  remove_2d_edge() {
    pcb_outline(layout()){
      case();
    }
  }
}

module left_pcb_traces() { // export left_pcb_traces.svg
  pcb_traces(layout()){
    case();
  }
}

module left_pcb_drill() { // export left_pcb_drill.svg
  pcb_drill(layout()){
    case();
  }
}
