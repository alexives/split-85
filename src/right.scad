include <common.scad>

module case() {
  translate([u_mm(0.5),-u_mm(1.5)])
    square([u_mm(12.5), u_mm(2.5)]);
  translate([0,u_mm(1)])
    square([u_mm(13), u_mm(1)]);
  translate([u_mm(0.25),u_mm(2)])
    square([u_mm(12.75), u_mm(1)]);
  translate([u_mm(0.75),u_mm(3)])
    square([u_mm(12.25), u_mm(2.25)]);
}

function layout() = [
  [[1,0.5],[],[],[],[],[],[2],[1,0.5],[],[],[]],
  [[],[],[],[],[],[],[],[1.5],[1,0.5],[],[],[]],
  [[1,0.25],[],[],[],[],[],[2.25],[1,0.5],[],[],[]],
  [[1,0.75],[],[],[],[],[1.75],[1,0.25,0.25],[1,0.25,-0.25],[],[],[]],
  [[2.75,0.75],[],[],[],[1,0.25,0.25],[],[],[1,0.25,-0.25],[],[]]
];

module remove_3d_edge() {
  difference(){
    children();
    translate([u_mm(-0.5),0,-20])
      linear_extrude(40)
        case_rm();
  }
}

module remove_2d_edge() {
  difference(){
    children();
    translate([u_mm(-0.5),0])
      case_rm();
  }
}

module right_board() { // export right_board.stl
  remove_3d_edge() {
    board(layout()){
      case();
    }
  }
}

module right_top_case() { // export right_top_case.stl
  remove_3d_edge() {
    top_case(layout(), margin=8, depth=6){
      case();
    }
  }
}

module right_bottom_case() { // export right_bottom_case.stl
  remove_3d_edge() {
    bottom_case(layout(), margin=8, depth=6){
      case();
    }
  }
}

module right_plate() { // export right_plate.svg
  remove_2d_edge() {
    top_plate_drawing(layout()){
      case();
    }
  }
}

module right_pcb_outline() { // export right_pcb_outline.svg
  remove_2d_edge() {
    pcb_outline(layout()){
      case();
    }
  }
}

module right_pcb_traces() { // export right_pcb_traces.svg
  pcb_traces(layout()){
    case();
  }
}

module right_pcb_drill() { // export right_pcb_drill.svg
  pcb_drill(layout()){
    case();
  }
}

right_pcb_outline();